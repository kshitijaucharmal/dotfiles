#!/bin/bash

picom --experimental-backend &
wal -R
killall polybar; polybar &

# open programs at startup
killall brave; brave &
killall alacritty; xdotool key Super+2; alacritty &
nautilus &
# youtube webapp (found this from .local/share/applications)
brave --app=http://www.youtube.com --class=WebApp-Youtube5733 --user-data-dir=/home/kshitij/.local/share/ice/profiles/Youtube5733 &

export QT_QPA_PLATFORMTHEME=qt5ct
