set tabstop=4 softtabstop=4
set shiftwidth=4
set expandtab
set smartindent
set nu
set exrc
set relativenumber
set nohlsearch
set hidden
set noerrorbells
set nowrap
set noswapfile
set nobackup
set undodir=~/.vim/undodir
set undofile
set incsearch
set scrolloff=8
set signcolumn=yes

let g:NERDTreeDirArrowExpandable = '>'
let g:NERDTreeDirArrowCollapsible = '*'

call plug#begin()
    Plug 'nvim-telescope/telescope.nvim'
    Plug 'junegunn/fzf', { 'dir' : '~/.fzf', 'do': './install --all' }
    Plug 'morhetz/gruvbox'
    Plug 'preservim/nerdtree'
call plug#end()

nnoremap <C-t> :NERDTreeToggle<CR>

syntax enable
highlight Normal guibg=none
colorscheme gruvbox
