#!/usr/bin/bash

languages=`echo "python java javascript c csharp" | tr ' ' '\n'`
core_utils=`echo "xargs find mv sed awk" | tr ' ' '\n'`

selected=`printf "$languages\n$core_utils" | fzf`
read -p "query: " query

if printf "$languages" | grep $selected; then
    alacritty -e bash -c "curl cht.sh/$selected/`echo $query | tr ' ' '+'` & read"
else
    alacritty -e bash -c "curl cht.sh/$selected~$query & while [ : ]; do sleep 1; done"
fi
