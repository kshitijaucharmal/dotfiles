if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

export ZSH="$HOME/.oh-my-zsh"

ZSH_THEME="robbyrussell"

plugins=( git zsh-syntax-highlighting zsh-autosuggestions )

source $ZSH/oh-my-zsh.sh

#[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

# Use powerline
USE_POWERLINE="true"

eval $(thefuck --alias)

alias ls='exa --icons'
alias pbcopy='xclip -sel clip'
alias f='fuck'
alias config='/usr/bin/git --git-dir=/home/kshitij/.cfg/ --work-tree=/home/kshitij'
alias vim='nvim'
alias htop='bpytop'

[[ -s /home/kshitij/.autojump/etc/profile.d/autojump.sh ]] && source /home/kshitij/.autojump/etc/profile.d/autojump.sh
unsetopt correct

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
export FZF_DEFAULT_OPS="--extended -s"
export PATH="/media/Data/android-studio-2021.2.1.15-linux/android-studio/bin:$PATH"
export EDITOR=vim
wal -R -q -e

#eval "$(starship init zsh)"
neofetch

